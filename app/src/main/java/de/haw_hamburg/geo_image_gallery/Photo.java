package de.haw_hamburg.geo_image_gallery;


import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;

public class Photo implements Serializable{

    private URI uri;
    private Long date;
    private float latitude;
    private float longitude;
    private float direction;



    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }

    public void setURI(URI uri) {
        this.uri = uri;
    }

    public URI getURI() {
        return uri;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getDate() {
        return date;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLongitude (float longitude) {
        this.longitude = longitude;
    }

    public float getLongitude() {
        return longitude;
    }
}
