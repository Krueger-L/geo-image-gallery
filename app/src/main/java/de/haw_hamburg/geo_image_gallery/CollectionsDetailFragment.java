package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.tumblr.bookends.Bookends;

import java.util.ArrayList;

import de.haw_hamburg.geo_image_gallery.ExtResources.ModifiedAlgorithm;

public class CollectionsDetailFragment extends Fragment implements PhotoListAdapter.PhotoClickListener, OnMapReadyCallback, GoogleMapClusterDialog.GoogleMapClusterDialogListener{

    private String collectionName;
    private PhotoManager photoManager;
    private ArrayList<Photo> photos = new ArrayList<>();
    private ArrayList<Photo> clusterPhotos = new ArrayList<>();
    private PhotoListAdapter adapter;
    private Bookends<PhotoListAdapter> bookendsAdapter;
    private PhotoListMapView mapView;
    private GoogleMap googleMap;
    private ClusterManager<PhotoClusterItem> clusterManager;
    private GoogleMapClusterDialog dialog;
    private Context context;
    private CollectionsDetailFragmentListener listener;

    public CollectionsDetailFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (CollectionsDetailFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement Listener");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PhotoDetailActivity.REQUEST_PHOTO_DETAIL_ACTIVITY) {
            photoManager.loadCollection();
            photos = photoManager.collections.getCollection(collectionName).getList();
            adapter.setData(photos);
            bookendsAdapter.notifyDataSetChanged();
        }
    }

    public static CollectionsDetailFragment newInstanceWithBundle(String collectionName) {
        CollectionsDetailFragment fragment = new CollectionsDetailFragment();
        Bundle args = new Bundle();
        args.putString(CollectionsDetailActivity.INTENT_EXTRA_PARAM_COLLECTION_NAME, collectionName);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            collectionName = getArguments().getString(CollectionsDetailActivity.INTENT_EXTRA_PARAM_COLLECTION_NAME, "");
        }

        //setup PhotoManger
        photoManager = new PhotoManager(getActivity());
        photoManager.loadSavedPhotos();
        photoManager.loadCollection();

        this.context = getContext();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_collections_detail, container, false);

        //get collection photos
        photos = photoManager.collections.getCollection(collectionName).getList();

        this.context = getContext();

        //setup recyclerView layout that has 3 items per row except the first item which gets full row
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (position ==0) ? 3 : 1;
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);

        //setup PhotoListAdapter
        adapter = new PhotoListAdapter(getContext(), photos, PhotoListAdapter.ADAPTER_TYPE_BOOKEND);
        adapter.setPhotoClickListener(this);
        //Wrap PhotoListAdapter into Bookends adapter
        bookendsAdapter = new Bookends<>(adapter);
        //setup mapView
        mapView = (PhotoListMapView) inflater.inflate(R.layout.photo_list_header, recyclerView, false);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                updateGoogleMap();
            }
        });
        //add mapview as first item to PhotoListAdapter via Bookends' addHeader method
        bookendsAdapter.addHeader(mapView);

        //Preload adapter items with Glide
        RecyclerViewPreloader<Photo> preloader = new RecyclerViewPreloader<>(GlideApp.with(getContext()), adapter, adapter.getPreloadSizeProvider(), 20);
        recyclerView.addOnScrollListener(preloader);
        recyclerView.setItemViewCacheSize(0);

        //adapter is configured -> set to recyclerView
        recyclerView.setAdapter(bookendsAdapter);

        //create GoogleMapClusterDialog for later use
        dialog = new GoogleMapClusterDialog();

        return rootView;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        photoManager.update();
        photos = photoManager.collections.getCollection(collectionName).getList();
        adapter.setData(photos);
        bookendsAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.collections_detail_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_rename:
                final EditText editText = new EditText(getActivity());
                editText.setText(collectionName);
                AlertDialog dia = new AlertDialog.Builder(getActivity())
                        .setTitle("Rename Collection")
                        .setView(editText)
                        .setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String tmpPath = editText.getText().toString();
                                boolean stringEmpty = true;
                                if (tmpPath.trim().length() > 0) {
                                    stringEmpty = false;
                                }
                                if (stringEmpty) {
                                    Toast.makeText(getActivity(), "Collection must have a name", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    boolean passed = photoManager.collections.checkNameAvailability(tmpPath);
                                    if (passed) {
                                        photoManager.collections.getCollection(collectionName).setName(tmpPath);
                                        photoManager.saveCollection();
                                        collectionName = tmpPath;
                                        listener.renameActionBarTitle(collectionName);

                                        Toast.makeText(getActivity(), "Collection saved", Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        Toast.makeText(getActivity(), "Collection already exists", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        })
                        .create();
                dia.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void requestDialogPhotos() {
        //interface method of GoogleMapClusterDialog
        dialog.setPhotos(clusterPhotos);
    }

    @Override
    public void onPhotoClicked(View view, int position) {
        //interface method of PhotoListAdapter -> start PhotoDetailActivity
        Photo photo = adapter.getPhoto(position-1);
        Intent intent = PhotoDetailActivity.getStartActivityIntent(context, photo.getURI().getPath(), photo.getDate(), photo.getLatitude(), photo.getLongitude(), photo.getDirection());
        startActivityForResult(intent, PhotoDetailActivity.REQUEST_PHOTO_DETAIL_ACTIVITY);
    }

    private void updateGoogleMap() {
        if (clusterManager != null) {
            clusterManager.clearItems();
            googleMap.clear();
            //apply padding to top of map to compensate for marker layout height
            googleMap.setPadding(0, 200, 0, 0);
            //add photos to googlemap and build bounds
            boolean hasItems = false;
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (int i = 0; i < photos.size(); i++) {
                hasItems = true;
                clusterManager.addItem(new PhotoClusterItem(new LatLng(photos.get(i).getLatitude(), photos.get(i).getLongitude()), photos.get(i).getURI()));
                builder.include(new LatLng((double) photos.get(i).getLatitude(), (double) photos.get(i).getLongitude()));
            }
            clusterManager.cluster();
            if (hasItems) {
                LatLngBounds bounds = builder.build();
                if ((mapView.getWidth() != 0) && (mapView.getHeight() != 0)) {
                    //default behaviour
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                }
                else {
                    //Fallback if maps Layout is not created
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 600, 600, 100));
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        //apply padding to top of map to compensate for marker layout height
        googleMap.setPadding(0, 200, 0, 0);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        //setup ClusterManager
        clusterManager = new ClusterManager<PhotoClusterItem>(getContext().getApplicationContext(), googleMap);
        clusterManager.setAlgorithm(new ModifiedAlgorithm<PhotoClusterItem>());
        clusterManager.setRenderer(new PhotoClusterItemRenderer(getContext().getApplicationContext(), googleMap, clusterManager));

        //set listeners
        googleMap.setOnCameraIdleListener(clusterManager);
        clusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<PhotoClusterItem>() {
            @Override
            public boolean onClusterClick(Cluster<PhotoClusterItem> cluster) {
                //clicked on cluster, generate new clusterPhotos for cluster dialog
                clusterPhotos.clear();
                ArrayList<Photo> photos = new ArrayList<>();
                PhotoClusterItem[] photoClusterItems = new PhotoClusterItem[cluster.getItems().size()];
                photoClusterItems = cluster.getItems().toArray(photoClusterItems);
                for(int i = 0; i < photoClusterItems.length; i++) {
                    for (int y = 0; y < CollectionsDetailFragment.this.photos.size(); y++) {
                        if(CollectionsDetailFragment.this.photos.get(y).getURI().getPath().equals(photoClusterItems[i].getUri().getPath())) {
                            photos.add(CollectionsDetailFragment.this.photos.get(y));
                        }
                    }
                }
                clusterPhotos = photos;
                //show cluster dialog
                dialog.show(CollectionsDetailFragment.this.getChildFragmentManager(), "CLUSTER_DIALOG");


                return true;
            }
        });
        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<PhotoClusterItem>() {
            @Override
            public boolean onClusterItemClick(PhotoClusterItem photoClusterItem) {
                //clicked on "single" photo marker -> open PhotoDetailActivity
                for (int i = 0; i < photos.size(); i++) {
                    //check for corresponding photo in photos list
                    if (photos.get(i).getURI().getPath().equals(photoClusterItem.getUri().getPath())) {
                        Intent intent = PhotoDetailActivity.getStartActivityIntent(context, photos.get(i).getURI().getPath(), photos.get(i).getDate(), photos.get(i).getLatitude(), photos.get(i).getLongitude(), photos.get(i).getDirection());
                        startActivityForResult(intent, PhotoDetailActivity.REQUEST_PHOTO_DETAIL_ACTIVITY);
                        break;
                    }
                }

                return true;
            }
        });
        googleMap.setOnMarkerClickListener(clusterManager);
    }

    public interface CollectionsDetailFragmentListener {
        public void renameActionBarTitle(String title);
    }
}
