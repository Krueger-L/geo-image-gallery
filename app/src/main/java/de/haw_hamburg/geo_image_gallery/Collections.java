package de.haw_hamburg.geo_image_gallery;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;


public class Collections implements Serializable {

    private ArrayList<Collection> list = new ArrayList<Collection>();

    public boolean checkNameAvailability(String name) {
        boolean availability = true;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(name)) {
                availability = false;
            }
        }
        return availability;
    }

    public ArrayList<Collection> getCollections() {
        return list;
    }

    public Collection getCollection(String name) {
        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(name)) {
                index = i;
            }
        }
        if (index >= 0) {
            return list.get(index);
        }
        else return null;
    }

    public ArrayList<String> getCorrespondingLists(String path) {
        ArrayList<String> output = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isInList(path)) {
                output.add(list.get(i).getName());
            }
        }
        return output;
    }

    public ArrayList<String> getSuitableLists(String path) {
        ArrayList<String> output = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).isInList(path)) {
                output.add(list.get(i).getName());
            }
        }
        return output;
    }
    public boolean isInCollections(String path) {
        boolean flag = false;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isInList(path)) {
                flag = true;
            }
        }
        return flag;
    }

    public void saveToCollection(String name, Photo photo) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(name)) {
                list.get(i).addPhoto(photo);
            }
        }
    }

    public void removeFromCollection(String name, Photo photo) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(name)) {
                list.get(i).removePhoto(photo.getURI(), photo.getDate());
            }
        }
    }

    public boolean addCollection(String name) {
        Collection collection = new Collection();
        collection.setName(name);
        boolean available = checkNameAvailability(name);
        if (available) {
            list.add(collection);
            return true;
        }
        else {
            return false;
        }
    }

    public void deleteCollection(String name) {
        int removableIndex = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(name)) {
                removableIndex = i;
            }
        }
        if (removableIndex >= 0) {
            list.remove(removableIndex);
        }
    }

    public boolean refreshList(ArrayList<Photo> allPhotos) {
        //check for widowed Collection Photos and remove them
        boolean changed = false;
        ArrayList<CollectionPhotoPair> deletionList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            for (int x = 0; x < list.get(i).getList().size(); x++) {
                boolean present = false;
                Photo tmpPhoto = list.get(i).getList().get(x);
                for (int y = 0; y < allPhotos.size(); y++) {
                    if (tmpPhoto.getURI().getPath().equals(allPhotos.get(y).getURI().getPath())) {
                        present = true;
                    }
                }
                if (!present) {
                    deletionList.add(new CollectionPhotoPair(list.get(i).getName(), tmpPhoto));
                    changed = true;
                }
            }
        }
        for (int i = 0; i < deletionList.size(); i++) {
            removeFromCollection(deletionList.get(i).collectionName, deletionList.get(i).photo);
        }
        return changed;
    }

    class CollectionPhotoPair {
        private Photo photo;
        private String collectionName;

        public CollectionPhotoPair(String collectionName, Photo photo) {
            this.collectionName = collectionName;
            this.photo = photo;
        }

    }
}
