package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.media.ExifInterface;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PhotoManager {

    private ArrayList<Photo> allPhotos = new ArrayList<>();
    public Collections collections = new Collections();
    private SavedPhotos savedPhotos = new SavedPhotos();
    private Context context;
    private static final String COLLECTIONS_FILENAME = "collections.ser";
    private static final String SAVEDPHOTOS_FILENAME = "savedphotos.ser";
    private static final String TIME_ZONE_BASE = "https://maps.googleapis.com/maps/api/timezone/json?location=";
    private static final String TIME_ZONE_TIMESTAMP = "&timestamp=";
    private static final String TIME_ZONE_KEY = "&key=AIzaSyAikC7Cxh9unjIy4bSdciw73I8aYGKWPK8";

    public PhotoManager(Context context) {
        this.context = context;
    }

    public ArrayList<Photo> getAllPhotos() {
        return savedPhotos.getArrayList();
    }

    public Collections getCollections() {
        return collections;
    }

    public SavedPhotos getSavedPhotos() {
        return savedPhotos;
    }

    public void update() {
        getPhotosFromDisk();
        loadSavedPhotos();
        loadCollection();
        //removes photos that are not longer present on device
        savedPhotos.refreshList(allPhotos);
        //update available photos in application according to device photos
        for (int i = 0; i < allPhotos.size(); i ++) {
            if (!savedPhotos.hasPhoto(allPhotos.get(i))) {
                Log.d(PhotoManager.class.getSimpleName(), "CREATE NETWORK REQUEST");
                try {
                    //open InputSteam
                    InputStream in;
                    in = context.getContentResolver().openInputStream(Uri.fromFile(new File(allPhotos.get(i).getURI())));

                    //create ExifInterface
                    ExifInterface exifInterface = new ExifInterface(in);

                    //get exif date/time
                    String dateStamp = exifInterface.getAttribute(ExifInterface.TAG_GPS_DATESTAMP);
                    String timeStamp = exifInterface.getAttribute(ExifInterface.TAG_GPS_TIMESTAMP);

                    //set date to photo
                    String dateTimeStamp = dateStamp + " " + timeStamp;
                    ParsePosition position = new ParsePosition(0);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                    Date dateTime = simpleDateFormat.parse(dateTimeStamp, position);
                    allPhotos.get(i).setDate(dateTime.getTime());

                    //request local time from GoogleMaps Timezone API
                    long date = allPhotos.get(i).getDate() / 1000;
                    HttpURLConnection connection = null;
                    JSONObject result = null;
                    StringBuilder jsonResults = new StringBuilder();
                    try {
                        String string = TIME_ZONE_BASE + allPhotos.get(i).getLatitude()+","+allPhotos.get(i).getLongitude()+TIME_ZONE_TIMESTAMP+date+TIME_ZONE_KEY;
                        URL url = new URL(string);
                        connection = (HttpURLConnection) url.openConnection();
                        InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
                        int read;
                        char[] buff = new char[1024];
                        while ((read = inputStreamReader.read(buff)) != -1) {
                            jsonResults.append(buff, 0, read);
                        }
                    } catch (MalformedURLException e) {
                        Log.e(PhotoManager.class.getSimpleName(), " Error processing URL", e);
                    } catch (IOException e) {
                        Log.e(PhotoManager.class.getSimpleName(), "Error connecting to Photomanager", e);
                    } finally {
                        if (connection != null) {
                            connection.disconnect();
                        }
                    }
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResults.toString());
                        result = jsonObject;
                    } catch (JSONException e) {
                        Log.e(PhotoManager.class.getSimpleName(), "Cannot process JSON results");
                    }
                    int dstOffset = 0;
                    int rawOffset = 0;
                    //process JSON result
                    try {
                        dstOffset = result.getInt("dstOffset");
                    } catch (JSONException e) {
                        Log.e(PhotoManager.class.getSimpleName(), "no value for dstOffset");
                    }
                    try {
                        rawOffset = result.getInt("rawOffset");
                    } catch (JSONException e) {
                        Log.e(PhotoManager.class.getSimpleName(), "no value for rawOffset");
                    }
                    //set localized time to photo
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date(allPhotos.get(i).getDate()));
                    calendar.add(Calendar.SECOND, (dstOffset + rawOffset));
                    allPhotos.get(i).setDate(calendar.getTime().getTime());

                    //add photo to available application photos
                    savedPhotos.addItem(allPhotos.get(i));

                    //close InputStream
                    in.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //Save available application photos
        saveSavedPhotos();

        //remove photos in collections that are not available anymore
        boolean changed = collections.refreshList(savedPhotos.getArrayList());
        if (changed) {
            saveCollection();
        }
    }

    private void getPhotosFromDisk() {
        //get all photos from device
        allPhotos.clear();
        try {
            //get external DCIM directory
            File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            //temporary file named path
            File path = new File("Filepath");

            //add all files to path given the corresponding folder name
            File[] files = file.listFiles();
            ArrayList<File> candidateList = new ArrayList<>();
            for (int x = 0; x < files.length; x++) {
                //common camera folder names (newer devices usually use "Camera")
                if ((files[x].getName().equals("Camera") || (files[x].getName().equals("100MEDIA"))) || (files[x].getName().equals("100ANDRO"))) {
                    if (files[x].listFiles().length > 0)
                        candidateList.add(files[x]);
                }
            }
            //get photo folder with most items
            File chosenCandidate = new File("");
            for (int i = 0; i <candidateList.size(); i++) {
                if (i == 0) {
                    chosenCandidate = candidateList.get(0);
                }
                else {
                    if (chosenCandidate.listFiles().length < candidateList.get(i).listFiles().length) {
                        chosenCandidate = candidateList.get(i);
                    }
                }
            }
            path = chosenCandidate;

            //process all photos in directory
            File[] photoList = path.listFiles();
            for (int i = 0; i < photoList.length; i++) {
                //check if file is of type .jpg
                if (photoList[i].getPath().substring(photoList[i].getPath().lastIndexOf('.') + 1).equals("jpg")) {
                    //create new photo and set path
                    Photo photo = new Photo();
                    photo.setURI(photoList[i].toURI());

                    //open InputSteam
                    InputStream in;
                    in = context.getContentResolver().openInputStream(Uri.fromFile(photoList[i]));
                    //create ExifInterface
                    ExifInterface exifInterface = new ExifInterface(in);

                    //get exif latitude and longitude
                    float[] res = new float[2];
                    exifInterface.getLatLong(res);
                    photo.setLatitude(res[0]);
                    photo.setLongitude(res[1]);

                    //get exif compass direction
                    float direction = (float) exifInterface.getAttributeDouble(ExifInterface.TAG_GPS_IMG_DIRECTION, 0);
                    photo.setDirection(direction);

                    //close InputStream
                    in.close();

                    //add photo to available device photos
                    allPhotos.add(photo);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveCollection() {
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(COLLECTIONS_FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(collections);
            objectOutputStream.close();
            fileOutputStream.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadCollection() {
        try {
            FileInputStream fileInputStream = context.openFileInput(COLLECTIONS_FILENAME);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            collections = (Collections) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveSavedPhotos() {
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(SAVEDPHOTOS_FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(savedPhotos);
            objectOutputStream.close();
            fileOutputStream.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadSavedPhotos() {
        try {
            FileInputStream fileInputStream = context.openFileInput(SAVEDPHOTOS_FILENAME);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            savedPhotos = (SavedPhotos) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
