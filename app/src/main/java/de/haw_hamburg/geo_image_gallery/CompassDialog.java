package de.haw_hamburg.geo_image_gallery;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class CompassDialog extends DialogFragment implements Compass.OnAzimuthChangedListener {

    private Compass compass;
    private CompassView compassView;
    private CompassDialogListener listener;

    public CompassDialog() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            listener = (CompassDialogListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(getParentFragment().toString() + "must implement CompassDialogListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_compass, container, false);
        compassView = (CompassView) rootView.findViewById(R.id.compassView);
        if (compass == null) {
            compass = new Compass(getContext(), this);
        }
        final Button cancelButton = (Button) rootView.findViewById(R.id.compass_dialog_cancel_button);
        final Button applyButton = (Button) rootView.findViewById(R.id.compass_dialog_apply_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close Dialog
                dismiss();
            }
        });
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send compass direction to parent -> close Dialog
                listener.selectedAzimuth(compassView.getCurrentAzimuth());
                dismiss();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //registers sensor listener
        compass.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        //removes sensor listener
        compass.stop();
    }

    @Override
    public void onAzimuthChanged(float azimuth) {
        //rotate compass view
        compassView.rotateCompass(azimuth);
    }

    public interface CompassDialogListener {
        void selectedAzimuth(float azimuth);
    }
}
