package de.haw_hamburg.geo_image_gallery;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.tumblr.bookends.Bookends;

import java.util.*;

import de.haw_hamburg.geo_image_gallery.ExtResources.ModifiedAlgorithm;

public class PhotosFragment extends Fragment implements PhotoListAdapter.PhotoClickListener, OnMapReadyCallback, GoogleMapClusterDialog.GoogleMapClusterDialogListener {

    private ArrayList<Photo> allPhotos = new ArrayList<>();
    private ArrayList<Photo> filteredPhotos = new ArrayList<>();
    private ArrayList<Photo> clusterPhotos = new ArrayList<>();
    private PhotosFragmentListener listener;
    private PhotoListAdapter adapter;
    private Bookends<PhotoListAdapter> bookendsAdapter;
    private PhotoListMapView mapView;
    private GoogleMap googleMap;
    private ClusterManager<PhotoClusterItem> clusterManager;
    private GoogleMapClusterDialog dialog;
    private Context context;

    public final String PREF_KEY_START_DATE = "pref_key_start_date"; //long
    public final String PREF_KEY_END_DATE = "pref_key_end_date"; //long
    public final String PREF_KEY_START_TIME = "pref_key_start_time"; //long
    public final String PREF_KEY_END_TIME = "pref_key_end_time"; //long
    public final String PREF_KEY_GPS_LAT = "pref_key_gps_lat"; //float
    public final String PREF_KEY_GPS_LNG = "pref_key_gps_lng"; //float
    public final String PREF_KEY_GPS_RADIUS = "pref_key_gps_radius"; //int
    public final String PREF_KEY_COMPASS_DIR = "pref_key_compass_dir"; //float
    public final String PREF_KEY_COMPASS_OFFSET = "pref_key_compass_offset"; //int


    public final String PREF_KEY_DATE_FLAG = "pref_key_date_flag";
    public final String PREF_KEY_TIME_FLAG = "pref_key_time_flag";
    public final String PREF_KEY_GPS_FLAG = "pref_key_gps_flag";
    public final String PREF_KEY_ORIENTATION_FLAG = "pref_key_orientation_flag";

    public PhotosFragment() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FilterActivity.REQUEST_FILTER_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                updatePhotoList();
                adapter.setData(filteredPhotos);
                bookendsAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onPhotoClicked(View view, int position) {
        Photo photo = adapter.getPhoto(position-1);
        Intent intent = PhotoDetailActivity.getStartActivityIntent(context, photo.getURI().getPath(), photo.getDate(), photo.getLatitude(), photo.getLongitude(), photo.getDirection());
        startActivityForResult(intent, PhotoDetailActivity.REQUEST_PHOTO_DETAIL_ACTIVITY);
    }

    private void updateGoogleMap() {
        if (clusterManager != null) {
            clusterManager.clearItems();
            googleMap.clear();
            //apply padding to top of map to compensate for marker layout height
            googleMap.setPadding(0, 200, 0, 0);
            boolean hasItems = false;
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (int i = 0; i < filteredPhotos.size(); i++) {
                hasItems = true;
                clusterManager.addItem(new PhotoClusterItem(new LatLng(filteredPhotos.get(i).getLatitude(), filteredPhotos.get(i).getLongitude()), filteredPhotos.get(i).getURI()));
                builder.include(new LatLng((double)filteredPhotos.get(i).getLatitude(), (double)filteredPhotos.get(i).getLongitude()));
            }
            clusterManager.cluster();
            if (hasItems) {
                LatLngBounds bounds = builder.build();
                if ((mapView.getWidth() != 0) && (mapView.getHeight() != 0)) {
                    //default behaviour
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                }
                else {
                    //Fallback if maps Layout is not created
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 600, 600, 100));
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        //apply padding to top of map to compensate for marker layout height
        googleMap.setPadding(0, 200, 0, 0);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        clusterManager = new ClusterManager<PhotoClusterItem>(getContext().getApplicationContext(), googleMap);
        clusterManager.setAlgorithm(new ModifiedAlgorithm<PhotoClusterItem>());
        clusterManager.setRenderer(new PhotoClusterItemRenderer(getContext().getApplicationContext(), googleMap, clusterManager));
        googleMap.setOnCameraIdleListener(clusterManager);

        clusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<PhotoClusterItem>() {
            @Override
            public boolean onClusterClick(Cluster<PhotoClusterItem> cluster) {
                clusterPhotos.clear();
                ArrayList<Photo> photos = new ArrayList<>();
                PhotoClusterItem[] photoClusterItems = new PhotoClusterItem[cluster.getItems().size()];
                photoClusterItems = cluster.getItems().toArray(photoClusterItems);
                for(int i = 0; i < photoClusterItems.length; i++) {
                    for (int y = 0; y < filteredPhotos.size(); y++) {
                        if(filteredPhotos.get(y).getURI().getPath().equals(photoClusterItems[i].getUri().getPath())) {
                            photos.add(filteredPhotos.get(y));
                        }
                    }
                }
                clusterPhotos = photos;
                dialog.show(PhotosFragment.this.getChildFragmentManager(), "CLUSTER_DIALOG");

                return true;
            }
        });
        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<PhotoClusterItem>() {
            @Override
            public boolean onClusterItemClick(PhotoClusterItem photoClusterItem) {
                for (int i = 0; i < filteredPhotos.size(); i++) {
                    if (filteredPhotos.get(i).getURI().getPath().equals(photoClusterItem.getUri().getPath())) {
                        Intent intent = PhotoDetailActivity.getStartActivityIntent(context, filteredPhotos.get(i).getURI().getPath(), filteredPhotos.get(i).getDate(), filteredPhotos.get(i).getLatitude(), filteredPhotos.get(i).getLongitude(), filteredPhotos.get(i).getDirection());
                        startActivityForResult(intent, PhotoDetailActivity.REQUEST_PHOTO_DETAIL_ACTIVITY);
                        break;
                    }
                }

                return true;
            }
        });
        googleMap.setOnMarkerClickListener(clusterManager);
    }

    @Override
    public void requestDialogPhotos() {
        dialog.setPhotos(clusterPhotos);
    }

    public interface PhotosFragmentListener {
        public void requestPhotos();
    }

    public void setAllPhotos(ArrayList<Photo> allPhotos) {
        this.allPhotos = allPhotos;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            listener = (PhotosFragmentListener) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + "must implement PhotosFragmentListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photos, container, false);
        listener.requestPhotos();
        updatePhotoList();
        this.context = getContext();

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (position ==0) ? 3 : 1;
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new PhotoListAdapter(getContext(), filteredPhotos, PhotoListAdapter.ADAPTER_TYPE_BOOKEND);
        adapter.setPhotoClickListener(this);
        bookendsAdapter = new Bookends<>(adapter);

        mapView = (PhotoListMapView) inflater.inflate(R.layout.photo_list_header, recyclerView, false);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                updateGoogleMap();
            }
        });

        bookendsAdapter.addHeader(mapView);

        RecyclerViewPreloader<Photo> preloader = new RecyclerViewPreloader<>(GlideApp.with(getContext()), adapter, adapter.getPreloadSizeProvider(), 20);

        recyclerView.addOnScrollListener(preloader);
        recyclerView.setItemViewCacheSize(0);
        recyclerView.setAdapter(bookendsAdapter);

        dialog = new GoogleMapClusterDialog();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void updatePhotoList() {
        filteredPhotos.clear();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        for (int i = 0; i < allPhotos.size(); i++) {
            boolean isValid = true;
            //Check Date
            if (preferences.getBoolean(PREF_KEY_DATE_FLAG, false)) {
                if ((allPhotos.get(i).getDate() >= preferences.getLong(PREF_KEY_START_DATE, 0)) && (allPhotos.get(i).getDate() <= preferences.getLong(PREF_KEY_END_DATE, 0))) {
                    //Valid
                }
                else {
                    isValid = false;
                }
            }
            //Check Time
            if (preferences.getBoolean(PREF_KEY_TIME_FLAG, false)) {
                Calendar startTime = Calendar.getInstance();
                Calendar endTime = Calendar.getInstance();
                Calendar photoTime = Calendar.getInstance();
                startTime.setTime(new Date(preferences.getLong(PREF_KEY_START_TIME, 0)));
                endTime.setTime(new Date(preferences.getLong(PREF_KEY_END_TIME, 0)));
                photoTime.setTime(new Date(allPhotos.get(i).getDate()));

                int photoHour = photoTime.get(Calendar.HOUR_OF_DAY);
                int photoMin = photoTime.get(Calendar.MINUTE);
                photoTime.setTime(new Date(0));
                photoTime.set(Calendar.HOUR_OF_DAY, photoHour);
                photoTime.set(Calendar.MINUTE, photoMin);
                int compare = startTime.compareTo(endTime);
                if (compare == 0) {
                    if (startTime.compareTo(photoTime) != 0) {
                        //start & end same but phototime is different
                        isValid = false;
                    }
                }
                else if (compare < 0) {
                    if (photoTime.after(startTime) && photoTime.before(endTime)) {
                        //phototime is valid
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    if (photoTime.after(startTime) || photoTime.before(endTime)) {
                        //phototime is valid
                    }
                    else {
                        isValid = false;
                    }
                }
            }
            //Check Gps
            if (preferences.getBoolean(PREF_KEY_GPS_FLAG, false)) {
                Location targetLocation = new Location("");
                Location photoLocation = new Location("");
                targetLocation.setLatitude((double)preferences.getFloat(PREF_KEY_GPS_LAT, 0));
                targetLocation.setLongitude((double)preferences.getFloat(PREF_KEY_GPS_LNG, 0));
                photoLocation.setLatitude((double) allPhotos.get(i).getLatitude());
                photoLocation.setLongitude((double) allPhotos.get(i).getLongitude());
                if (targetLocation.distanceTo(photoLocation) > (float) preferences.getInt(PREF_KEY_GPS_RADIUS, 0)) {
                    isValid = false;
                }
            }
            //Check Direction
            if (preferences.getBoolean(PREF_KEY_ORIENTATION_FLAG, false)) {
                float targetDir = preferences.getFloat(PREF_KEY_COMPASS_DIR, 0);
                int offset = preferences.getInt(PREF_KEY_COMPASS_OFFSET, 0);
                float targetStartSpan = targetDir - offset;
                float targetEndSpan = targetDir + offset;
                if (targetStartSpan < 0) {
                    targetStartSpan = targetStartSpan + 360;
                }
                if (targetEndSpan >= 360) {
                    targetEndSpan = targetEndSpan -360;
                }
                if (targetStartSpan < targetEndSpan) {
                    if ((allPhotos.get(i).getDirection() >= targetStartSpan) && (allPhotos.get(i).getDirection() <= targetEndSpan)) {
                        //valid
                    }
                    else {
                        isValid = false;
                    }
                }
                else if (targetStartSpan > targetEndSpan) {
                    if ((allPhotos.get(i).getDirection() >= targetStartSpan) || (allPhotos.get(i).getDirection() <= targetEndSpan)) {
                        //valid
                    }
                    else {
                        isValid = false;
                    }
                }
            }
            if (isValid) {
                filteredPhotos.add(allPhotos.get(i));
            }
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        listener.requestPhotos();
        updatePhotoList();
        adapter.setData(filteredPhotos);
        bookendsAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.photos_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_photos_settings:
                Intent intent = FilterActivity.getStartActivityIntent(this.getContext());
                startActivityForResult(intent, FilterActivity.REQUEST_FILTER_ACTIVITY);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
