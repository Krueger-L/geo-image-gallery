package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Compass implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor rotationVectorSensor;
    private OnAzimuthChangedListener onAzimuthChangedListener;

    public Compass(Context context, OnAzimuthChangedListener listener) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        onAzimuthChangedListener = listener;
    }

    public void start() {
        sensorManager.registerListener(this, rotationVectorSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    public void stop() {
        sensorManager.unregisterListener(this);
    }

    private float calculateAzimuth(float[] rotationMatrix) {
        float orientation[] = new float[3];
        SensorManager.getOrientation(rotationMatrix, orientation);
        float i = (float) Math.toDegrees(orientation[0]);
        i = (i + 360) % 360;
        return i;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        //get sensor data for Sensor.TYPE_ROTATION_VECTOR
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            float[] rotationMatrix = new float[9];
            SensorManager.getRotationMatrixFromVector(rotationMatrix, sensorEvent.values);
            onAzimuthChangedListener.onAzimuthChanged(calculateAzimuth(rotationMatrix));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public interface OnAzimuthChangedListener {
        void onAzimuthChanged(float azimuth);
    }
}
