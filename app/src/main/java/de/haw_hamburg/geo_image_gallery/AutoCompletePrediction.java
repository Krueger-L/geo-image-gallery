package de.haw_hamburg.geo_image_gallery;


public class AutoCompletePrediction {

    private final String placeId;
    private final String description;

    public AutoCompletePrediction(String placeId, String description) {
        this.placeId = placeId;
        this.description = description;
    }


    public String getPlaceId()
    {
        return placeId;
    }


    public String getDescription()
    {
        return description;
    }
}
