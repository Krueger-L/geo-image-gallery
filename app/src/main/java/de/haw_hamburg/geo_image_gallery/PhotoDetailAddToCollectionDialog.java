package de.haw_hamburg.geo_image_gallery;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

public class PhotoDetailAddToCollectionDialog extends DialogFragment implements PhotoDetailAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private PhotoDetailAdapter adapter;
    private PhotoDetailAddToCollectionDialogListener listener;

    public PhotoDetailAddToCollectionDialog() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            listener = (PhotoDetailAddToCollectionDialogListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(getParentFragment().toString() + "must implement listener");
        }
        adapter = new PhotoDetailAdapter(PhotoDetailAdapter.TYPE_DIALOG);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        listener.addDialogDismissed();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_photo_detail_add_to_collection, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.collection_list);
        Button button = (Button) rootView.findViewById(R.id.cancel_dialog_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        adapter.setOnItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listener.receiveStrings();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.dismissAllowingStateLoss();
    }

    @Override
    public void onItemClicked(String collectionString) {
        listener.collectionSelected(collectionString);
        dismiss();
    }

    public interface PhotoDetailAddToCollectionDialogListener {
        public void receiveStrings();
        public void collectionSelected(String string);
        public void addDialogDismissed();
    }

    public void setStringsToAdapter(ArrayList<String> strings) {
        adapter.setCollectionStrings(strings);
    }
}
