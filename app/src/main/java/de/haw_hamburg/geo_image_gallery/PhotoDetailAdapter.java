package de.haw_hamburg.geo_image_gallery;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PhotoDetailAdapter extends RecyclerView.Adapter<PhotoDetailAdapter.ViewHolder> {

    private List<String> collectionStrings;
    private OnItemClickListener listener;
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_DIALOG = 1;
    private int adapterType;


    public PhotoDetailAdapter(int type)
    {
        this.collectionStrings = new ArrayList<>();
        adapterType = type;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.collection_list_item,
                parent,
                false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        String collectionString = collectionStrings.get(position);
        holder.setPlaceAutocompletePrediction(collectionString);
        holder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                listener.onItemClicked(holder.collectionString);
            }
        });
    }


    @Override
    public int getItemCount()
    {
        return collectionStrings.size();
    }


    public void setCollectionStrings(List<String> collectionStrings)
    {
        this.collectionStrings = collectionStrings;
        notifyDataSetChanged();
    }


    public String getCollectionString(int position)
    {
        return collectionStrings.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        this.listener = listener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private View view;
        private TextView textView;
        private String collectionString;
        private ImageView imageView;


        public ViewHolder(View itemView)
        {
            super(itemView);
            this.view = itemView;
            textView = (TextView) itemView.findViewById(R.id.text);
            imageView = (ImageView) itemView.findViewById(R.id.delete);
            if (adapterType == TYPE_DIALOG) {
                imageView.setVisibility(View.GONE);
            }
        }


        public void setOnClickListener(View.OnClickListener listener)
        {
            if (adapterType == TYPE_NORMAL) {
                imageView.setOnClickListener(listener);
            }
            else if (adapterType == TYPE_DIALOG) {
                view.setOnClickListener(listener);
            }
        }


        public void setPlaceAutocompletePrediction(String collectionString)
        {
            this.collectionString = collectionString;
            textView.setText(collectionString);
        }
    }

    public interface OnItemClickListener
    {
        void onItemClicked(String collectionString);
    }
}
