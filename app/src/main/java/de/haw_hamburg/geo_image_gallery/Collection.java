package de.haw_hamburg.geo_image_gallery;



import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;

public class Collection implements Serializable{

    private String name;
    private ArrayList<Photo> list = new ArrayList<Photo>();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setList(ArrayList<Photo> list) {
        this.list = list;
    }

    public ArrayList<Photo> getList() {
        return list;
    }

    public boolean isInList (String path) {
        boolean check = false;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getURI().getPath().equals(path)) {
                check = true;
            }
        }
        return check;
    }

    public void addPhoto(Photo photo) {
        if (!isInList(photo.getURI().getPath())) {
            list.add(photo);
        }
    }

    public void removePhoto(URI uri, Long date) {
        int removableIndex = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getURI().getPath().equals(uri.getPath())) {
                removableIndex = i;
            }
        }
        if (removableIndex >= 0) {
            list.remove(removableIndex);
        }
    }

}
