package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PhotoDetailFragment extends Fragment implements OnMapReadyCallback, PhotoDetailAdapter.OnItemClickListener, PhotoDetailAddToCollectionDialog.PhotoDetailAddToCollectionDialogListener {

    private String path;
    private Long date;
    private float latitude;
    private float longitude;
    private float direction;
    private Calendar calendar;
    private ImageView imageView;
    private PhotoListMapView mapView;
    private CompassView compassView;
    private TextView textViewDate;
    private GoogleMap googleMap;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PhotoDetailAdapter adapter;
    private PhotoDetailAddToCollectionDialog photoDetailAddToCollectionDialog;
    private PhotoManager photoManager;
    private TextView placeHolderTextView;
    private LinearLayout linearLayoutContainer1;
    private LinearLayout linearLayoutContainer2;
    private RelativeLayout linearLayoutContainer3;
    private LinearLayout linearLayoutContainer4;
    private ScrollView scrollView;
    private PhotoDetailFragmentListener listener;
    private android.support.v7.widget.ShareActionProvider shareActionProvider;

    public PhotoDetailFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (PhotoDetailFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement Listener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calendar = Calendar.getInstance();
        if (getArguments() != null) {
            path = getArguments().getString(PhotoDetailActivity.INTENT_EXTRA_PATH, "");
            date = getArguments().getLong(PhotoDetailActivity.INTENT_EXTRA_DATE, 0);
            latitude = getArguments().getFloat(PhotoDetailActivity.INTENT_EXTRA_LAT, 0);
            longitude = getArguments().getFloat(PhotoDetailActivity.INTENT_EXTRA_LNG, 0);
            direction = getArguments().getFloat(PhotoDetailActivity.INTENT_EXTRA_DIR, 0);
        }

        //setup PhotoManager
        photoManager = new PhotoManager(getActivity());
        photoManager.loadSavedPhotos();
        photoManager.loadCollection();

        photoDetailAddToCollectionDialog = new PhotoDetailAddToCollectionDialog();
        calendar.setTime(new Date(date));
        setHasOptionsMenu(true);
    }

    public static  PhotoDetailFragment newInstanceWithBundle(String path, Long date, float latitude, float longitude, float direction) {
        PhotoDetailFragment fragment = new PhotoDetailFragment();
        Bundle args = new Bundle();
        args.putString(PhotoDetailActivity.INTENT_EXTRA_PATH, path);
        args.putLong(PhotoDetailActivity.INTENT_EXTRA_DATE, date);
        args.putFloat(PhotoDetailActivity.INTENT_EXTRA_LAT, latitude);
        args.putFloat(PhotoDetailActivity.INTENT_EXTRA_LNG, longitude);
        args.putFloat(PhotoDetailActivity.INTENT_EXTRA_DIR, direction);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_detail, container, false);
        linearLayoutContainer1 = (LinearLayout) rootView.findViewById(R.id.linearContainer1);
        linearLayoutContainer2 = (LinearLayout) rootView.findViewById(R.id.linearContainer2);
        linearLayoutContainer3 = (RelativeLayout) rootView.findViewById(R.id.linearContainer3);
        linearLayoutContainer4 = (LinearLayout) rootView.findViewById(R.id.linearContainer4);
        imageView = (ImageView) rootView.findViewById(R.id.image);
        mapView = (PhotoListMapView) rootView.findViewById(R.id.mapView);
        compassView = (CompassView) rootView.findViewById(R.id.compassView);
        textViewDate = (TextView) rootView.findViewById(R.id.tvDate);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        placeHolderTextView = (TextView) rootView.findViewById(R.id.textViewPlaceholder);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);
        Button addButton = (Button) rootView.findViewById(R.id.photo_detail_fragment_add_button);
        Button createButton = (Button) rootView.findViewById(R.id.photo_detail_fragment_create_button);
        Button navigationButton = (Button) rootView.findViewById(R.id.photo_detail_fragment_navigation_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show PhotoDetailAddToCollectionDialog if available
                ArrayList<String> arrayList = photoManager.collections.getSuitableLists(path);
                if (arrayList.size() > 0) {
                    photoDetailAddToCollectionDialog.show(PhotoDetailFragment.this.getChildFragmentManager(), "photoDetailAddToCollectionDialog");
                }
                else {
                    Toast.makeText(getActivity(), "No Collection available", Toast.LENGTH_LONG).show();
                }
            }
        });
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create new AlertDialog for collection creation
                final EditText editText = new EditText(getActivity());
                AlertDialog dia = new AlertDialog.Builder(getActivity())
                        .setTitle("Create new Collection")
                        .setView(editText)
                        .setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String tmpPath = editText.getText().toString();
                                boolean stringEmpty = true;
                                if (tmpPath.trim().length() > 0) {
                                    stringEmpty = false;
                                }
                                if (stringEmpty) {
                                    Toast.makeText(getActivity(), "Collection must have a name", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    boolean passed = photoManager.collections.addCollection(tmpPath);
                                    if (passed) {
                                        photoManager.collections.saveToCollection(tmpPath, photoManager.getSavedPhotos().getPhoto(path));
                                        photoManager.saveCollection();
                                        adapter.setCollectionStrings(photoManager.collections.getCorrespondingLists(path));
                                        needPlaceHolder();
                                        Toast.makeText(getActivity(), "Collection saved", Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        Toast.makeText(getActivity(), "Collection already exists", Toast.LENGTH_LONG).show();
                                    }
                                }
                                listener.hideStatusBar();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                listener.hideStatusBar();
                            }
                        })
                        .create();
                dia.show();
            }
        });
        navigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start external app for navigation
                Uri uri = Uri.parse("google.navigation:q="+latitude+","+longitude);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });

        adapter = new PhotoDetailAdapter(PhotoDetailAdapter.TYPE_NORMAL);
        adapter.setOnItemClickListener(this);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        adapter.setCollectionStrings(photoManager.collections.getCorrespondingLists(path));
        needPlaceHolder();

        compassView.rotateCompass(direction);

        DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        textViewDate.setText(dateFormat.format(calendar.getTime()));

        mapView.onCreate(null);
        mapView.getMapAsync(this);

        //load Image
        GlideApp.with(getActivity())
                .load(path)
                .centerInside()
                .into(imageView);

        //check for device orientation and change layout accordingly
        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //show views
            mapView.setVisibility(View.VISIBLE);
            compassView.setVisibility(View.VISIBLE);
            navigationButton.setVisibility(View.VISIBLE);
            linearLayoutContainer1.setVisibility(View.VISIBLE);
            linearLayoutContainer2.setVisibility(View.VISIBLE);
            linearLayoutContainer3.setVisibility(View.VISIBLE);
            linearLayoutContainer4.setVisibility(View.VISIBLE);
            //set height of imageView to display height
            Point size = new Point();
            getActivity().getWindowManager().getDefaultDisplay().getSize(size);
            int fullScreenHeight = size.y;
            ViewGroup.LayoutParams params = imageView.getLayoutParams();
            params.height = fullScreenHeight;
        }
        else {
            mapView.setVisibility(View.GONE);
            compassView.setVisibility(View.GONE);
            navigationButton.setVisibility(View.GONE);
            linearLayoutContainer1.setVisibility(View.GONE);
            linearLayoutContainer2.setVisibility(View.GONE);
            linearLayoutContainer3.setVisibility(View.GONE);
            linearLayoutContainer4.setVisibility(View.GONE);
            //set height of imageView to display height
            Point size = new Point();
            getActivity().getWindowManager().getDefaultDisplay().getSize(size);
            int fullScreenHeight = size.y;
            ViewGroup.LayoutParams params = imageView.getLayoutParams();
            params.height = fullScreenHeight;
        }

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                //hide or show action bar according to scroll value
                int posY = scrollView.getScrollY();
                if (posY > 600) {
                    listener.showActionBar(true);
                }
                else if ((posY > 0) && (posY < 600)) {
                    //do nothing
                }
                else {
                    listener.showActionBar(false);
                }
                listener.hideStatusBar();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        photoManager.update();
        if (photoManager.getSavedPhotos().getPhoto(path) == null) {
            getActivity().finish();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.photo_detail_fragment_menu, menu);

        //setup image sharing
        MenuItem item = menu.findItem(R.id.menu_item_share);
        shareActionProvider = (android.support.v7.widget.ShareActionProvider) MenuItemCompat.getActionProvider(item);
        //create shareIntent
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");
        Uri uri = Uri.fromFile(new File(path));
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        //set Intent to ShareActionProvider
        shareActionProvider.setShareIntent(intent);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.googleMap = map;
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //return true so default behaviour is not triggered
                return true;
            }
        });
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng((double) latitude, (double) longitude), 10));
        googleMap.addMarker(new MarkerOptions().position(new LatLng((double) latitude, (double) longitude)));
    }

    @Override
    public void receiveStrings() {
        photoDetailAddToCollectionDialog.setStringsToAdapter(photoManager.collections.getSuitableLists(path));
    }

    @Override
    public void collectionSelected(String string) {
        //add item to selected collection
        photoManager.collections.saveToCollection(string, photoManager.getSavedPhotos().getPhoto(path));
        photoManager.saveCollection();
        adapter.setCollectionStrings(photoManager.collections.getCorrespondingLists(path));
        needPlaceHolder();
    }

    @Override
    public void addDialogDismissed() {
        listener.hideStatusBar();
    }

    @Override
    public void onItemClicked(String collectionString) {
        //remove item from selected collection
        photoManager.collections.removeFromCollection(collectionString, photoManager.getSavedPhotos().getPhoto(path));
        photoManager.saveCollection();
        adapter.setCollectionStrings(photoManager.collections.getCorrespondingLists(path));
        needPlaceHolder();
    }

    private void needPlaceHolder() {
        if (adapter.getItemCount() > 0) {
            placeHolderTextView.setVisibility(View.GONE);
        }
        else {
            placeHolderTextView.setVisibility(View.VISIBLE);
        }
    }

    public interface PhotoDetailFragmentListener {
        public void showActionBar(boolean bool);
        public void hideStatusBar();
    }
}
