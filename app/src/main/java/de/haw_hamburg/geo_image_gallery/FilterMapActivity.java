package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class FilterMapActivity extends AppCompatActivity {

    public static final int REQUEST_FILTER_MAP_ACTIVITY = 1002;
    public static final String INTENT_EXTRA_PARAM_LAT = "intentLat";
    public static final String INTENT_EXTRA_PARAM_LNG = "intentLng";

    public static Intent getStartActivityIntent(Context context, double lat, double lng) {
        Intent intent = new Intent(context, FilterMapActivity.class);
        intent.putExtra(INTENT_EXTRA_PARAM_LAT, lat);
        intent.putExtra(INTENT_EXTRA_PARAM_LNG, lng);

        return intent;
    }

    public static void startActivity(Context context) {
        context.startActivity(getStartActivityIntent(context, 0, 0));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        setContentView(R.layout.activity_filter_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_clear_24dp);
        if (savedInstanceState == null) {
            //add fragment to layout container
            FilterMapFragment fragment = FilterMapFragment.newInstanceWithBundle(getIntent().getDoubleExtra(INTENT_EXTRA_PARAM_LAT, 0), getIntent().getDoubleExtra(INTENT_EXTRA_PARAM_LNG, 0));
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
