package de.haw_hamburg.geo_image_gallery;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.util.ViewPreloadSizeProvider;

import java.util.ArrayList;
import java.util.List;


public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.ViewHolderItem> implements ListPreloader.PreloadModelProvider<Photo> {

    private ArrayList<Photo> data = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private PhotoClickListener photoClickListener;
    private ViewPreloadSizeProvider<Photo> preloadSizeProvider = new ViewPreloadSizeProvider<>();
    public static final int ADAPTER_TYPE_NORMAL = 1;
    public static final int ADAPTER_TYPE_BOOKEND = 2;
    private int adapterType;

    public PhotoListAdapter(Context context, ArrayList<Photo> data, int type) {
        this.context = context;
        this.data = data;
        this. inflater = LayoutInflater.from(context);
        adapterType = type;
    }

    public void setData(ArrayList<Photo> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ViewPreloadSizeProvider<Photo> getPreloadSizeProvider() {
        return preloadSizeProvider;
    }


    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.photo_list_item, parent, false);
        ViewHolderItem viewHolderItem = new ViewHolderItem(view);
        preloadSizeProvider.setView(viewHolderItem.imageView);
        return viewHolderItem;
    }


    @Override
    public void onBindViewHolder(ViewHolderItem holder, int position) {
        ImageView imageView = holder.imageView;
        //load image with Glide
        GlideApp.with(context)
                .load(data.get(position).getURI().getPath())
                .override(300, 300)
                .centerCrop()
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Photo getPhoto (int position) {
        return data.get(position);
    }

    @NonNull
    @Override
    public List<Photo> getPreloadItems(int position) {
        //supply preload Item for Glide Preloader
        if (position != 0 && adapterType == ADAPTER_TYPE_BOOKEND) {
            return java.util.Collections.singletonList(data.get(position-1));
        }
        else if (position != 0 && adapterType == ADAPTER_TYPE_NORMAL) {
            return java.util.Collections.singletonList(data.get(position));
        }
        else {
            return java.util.Collections.singletonList(null);
        }
    }

    @Nullable
    @Override
    public RequestBuilder getPreloadRequestBuilder(Photo item) {
        //preload item with Glide
        return GlideApp.with(context)
                .load(item.getURI().getPath())
                .override(300, 300)
                .centerCrop();
    }

    public interface PhotoClickListener {
        void onPhotoClicked (View view, int position);
    }

    public void setPhotoClickListener( PhotoClickListener photoClickListener) {
        this.photoClickListener = photoClickListener;
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;

        ViewHolderItem(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (photoClickListener != null) {
                photoClickListener.onPhotoClicked(v, getAdapterPosition());
            }
        }
    }
}