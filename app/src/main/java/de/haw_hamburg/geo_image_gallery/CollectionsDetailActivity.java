package de.haw_hamburg.geo_image_gallery;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


public class CollectionsDetailActivity extends AppCompatActivity implements CollectionsDetailFragment.CollectionsDetailFragmentListener {

    public static int REQUEST_COLLECTIONS_DETAIL_ACTIVITY = 1014;
    public static String INTENT_EXTRA_PARAM_COLLECTION_NAME = "intent_extra_param_collection_name";

    public static Intent getStartActivityIntent(Context context, String collectionName) {
        Intent intent = new Intent(context, CollectionsDetailActivity.class);
        intent.putExtra(INTENT_EXTRA_PARAM_COLLECTION_NAME, collectionName);
        return intent;
    }

    public static void startActivity(Context context, String collectionName) {
        context.startActivity(getStartActivityIntent(context, collectionName));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        setContentView(R.layout.activity_collections_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra(INTENT_EXTRA_PARAM_COLLECTION_NAME));
        if (savedInstanceState == null) {
            //add fragment to layout container
            CollectionsDetailFragment fragment = CollectionsDetailFragment.newInstanceWithBundle(getIntent().getStringExtra(INTENT_EXTRA_PARAM_COLLECTION_NAME));
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void renameActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
