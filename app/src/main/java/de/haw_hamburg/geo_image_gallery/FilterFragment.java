package de.haw_hamburg.geo_image_gallery;


import android.app.Activity;
import android.app.DatePickerDialog;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FilterFragment extends Fragment implements CompassDialog.CompassDialogListener {

    private EditText textViewStartDate;
    private EditText textViewEndDate;
    private EditText textViewStartTime;
    private EditText textViewEndTime;
    private EditText textViewGpsCoords;
    private LooseFocusEditText textViewRadius;
    private EditText textViewDirection;
    private LooseFocusEditText textViewDirectionOffset;
    private ImageView deleteIconDate;
    private ImageView deleteIconTime;
    private ImageView deleteIconGps;
    private ImageView deleteIconCompass;
    private Button applyButton;
    private Calendar calendarStartDate;
    private Calendar calendarEndDate;
    private Calendar calendarStartTime;
    private Calendar calendarEndTime;
    private Context context;

    private double latitude;
    private double longitude;
    private float azimuth;

    private CompassDialog compassDialog;

    private boolean dateFlag;
    private boolean timeFlag;
    private boolean gpsFlag;
    private boolean orientationFlag;

    public final String PREF_KEY_START_DATE = "pref_key_start_date"; //long
    public final String PREF_KEY_END_DATE = "pref_key_end_date"; //long
    public final String PREF_KEY_START_TIME = "pref_key_start_time"; //long
    public final String PREF_KEY_END_TIME = "pref_key_end_time"; //long
    public final String PREF_KEY_GPS_LAT = "pref_key_gps_lat"; //float
    public final String PREF_KEY_GPS_LNG = "pref_key_gps_lng"; //float
    public final String PREF_KEY_GPS_RADIUS = "pref_key_gps_radius"; //int
    public final String PREF_KEY_COMPASS_DIR = "pref_key_compass_dir"; //float
    public final String PREF_KEY_COMPASS_OFFSET = "pref_key_compass_offset"; //int


    public final String PREF_KEY_DATE_FLAG = "pref_key_date_flag";
    public final String PREF_KEY_TIME_FLAG = "pref_key_time_flag";
    public final String PREF_KEY_GPS_FLAG = "pref_key_gps_flag";
    public final String PREF_KEY_ORIENTATION_FLAG = "pref_key_orientation_flag";


    public FilterFragment() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FilterMapActivity.REQUEST_FILTER_MAP_ACTIVITY && resultCode == Activity.RESULT_OK) {
            //get result data of FilterMapActivity
            latitude = data.getDoubleExtra(FilterMapActivity.INTENT_EXTRA_PARAM_LAT, 0);
            longitude = data.getDoubleExtra(FilterMapActivity.INTENT_EXTRA_PARAM_LNG, 0);
            Toast.makeText(getContext(), "Lat "+latitude+" Lng "+longitude, Toast.LENGTH_LONG).show();
            textViewGpsCoords.setText(String.format("%.1f", latitude)+" / "+String.format("%.1f", longitude));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filter, container, false);
        textViewStartDate = (EditText) rootView.findViewById(R.id.tvStartDate);
        textViewEndDate = (EditText) rootView.findViewById(R.id.tvEndDate);
        textViewStartTime = (EditText) rootView.findViewById(R.id.tvStartTime);
        textViewEndTime = (EditText) rootView.findViewById(R.id.tvEndTime);
        textViewGpsCoords = (EditText) rootView.findViewById(R.id.tvGpsCoords);
        textViewRadius = (LooseFocusEditText) rootView.findViewById(R.id.tvRadius);
        textViewDirection = (EditText) rootView.findViewById(R.id.tvCompassDirection);
        textViewDirectionOffset = (LooseFocusEditText) rootView.findViewById(R.id.tvDegree);
        deleteIconDate = (ImageView) rootView.findViewById(R.id.dateDeleteIcon);
        deleteIconTime = (ImageView) rootView.findViewById(R.id.timeDeleteIcon);
        deleteIconGps = (ImageView) rootView.findViewById(R.id.gpsDeleteIcon);
        deleteIconCompass = (ImageView) rootView.findViewById(R.id.compassDeleteIcon);
        applyButton = (Button) rootView.findViewById(R.id.filter_fragment_apply_button);


        compassDialog = new CompassDialog();
        context = this.getContext();

        calendarStartDate = Calendar.getInstance();
        calendarEndDate = Calendar.getInstance();
        calendarStartTime = Calendar.getInstance();
        calendarEndTime = Calendar.getInstance();

        textViewGpsCoords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start FilterMapActivity
                Intent intent = FilterMapActivity.getStartActivityIntent(context, latitude, longitude);
                startActivityForResult(intent, FilterMapActivity.REQUEST_FILTER_MAP_ACTIVITY);
            }
        });

        textViewDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show compass Dialog
                compassDialog.show(FilterFragment.this.getChildFragmentManager(), "COMPASS_DIALOG");
            }
        });

        textViewRadius.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            //Close Keyboard on Enter/Done and remove Focus from textViewRadius
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    textViewRadius.clearFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(textViewRadius.getWindowToken(), 0);
                }
                return true;
            }
        });

        textViewDirectionOffset.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            //Close Keyboard on Enter/Done and remove Focus from textViewDirectionOffset
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    textViewDirectionOffset.clearFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(textViewDirectionOffset.getWindowToken(), 0);
                }
                return true;
            }
        });

        textViewStartDate.setOnClickListener(new View.OnClickListener() {
            //Open DatePickerDialog and set corresponding Calender/EditText
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendarStartDate.set(Calendar.YEAR, year);
                        calendarStartDate.set(Calendar.MONTH, month);
                        calendarStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        calendarStartDate.set(Calendar.HOUR_OF_DAY, 0);
                        calendarStartDate.set(Calendar.MINUTE, 0);
                        calendarStartDate.set(Calendar.SECOND, 0);
                        calendarStartDate.set(Calendar.MILLISECOND, 0);
                        String format = "dd/MM/yy";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
                        textViewStartDate.setText(simpleDateFormat.format(calendarStartDate.getTime()));

                    }
                }, calendarStartDate.get(Calendar.YEAR), calendarStartDate.get(Calendar.MONTH), calendarStartDate.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        textViewEndDate.setOnClickListener(new View.OnClickListener() {
            //Open DatePickerDialog and set corresponding Calender/EditText
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendarEndDate.set(Calendar.YEAR, year);
                        calendarEndDate.set(Calendar.MONTH, month);
                        calendarEndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        calendarEndDate.set(Calendar.HOUR_OF_DAY, 23);
                        calendarEndDate.set(Calendar.MINUTE, 59);
                        calendarEndDate.set(Calendar.SECOND, 59);
                        calendarEndDate.set(Calendar.MILLISECOND, 999);
                        String format = "dd/MM/yy";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
                        textViewEndDate.setText(simpleDateFormat.format(calendarEndDate.getTime()));

                    }
                }, calendarEndDate.get(Calendar.YEAR), calendarEndDate.get(Calendar.MONTH), calendarEndDate.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        textViewStartTime.setOnClickListener(new View.OnClickListener() {
            //Open TimePickerDialog and set corresponding Calender/EditText
            @Override
            public void onClick(View v) {
                TimePickerDialog dialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendarStartTime.setTime(new Date(0));
                        calendarStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendarStartTime.set(Calendar.MINUTE, minute);
                        String format = "HH:mm";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
                        textViewStartTime.setText(simpleDateFormat.format(calendarStartTime.getTime())+ " h");
                    }
                }, calendarStartTime.get(Calendar.HOUR_OF_DAY), calendarStartTime.get(Calendar.MINUTE), true);
                dialog.show();
            }
        });

        textViewEndTime.setOnClickListener(new View.OnClickListener() {
            //Open TimePickerDialog and set corresponding Calender/EditText
            @Override
            public void onClick(View v) {
                TimePickerDialog dialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendarEndTime.setTime(new Date(0));
                        calendarEndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendarEndTime.set(Calendar.MINUTE, minute);
                        String format = "HH:mm";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
                        textViewEndTime.setText(simpleDateFormat.format(calendarEndTime.getTime()) + " h");
                    }
                }, calendarEndTime.get(Calendar.HOUR_OF_DAY), calendarEndTime.get(Calendar.MINUTE), true);
                dialog.show();
            }
        });

        deleteIconDate.setOnClickListener(new View.OnClickListener() {
            //Clear data
            @Override
            public void onClick(View v) {
                textViewStartDate.setText("");
                textViewEndDate.setText("");
                calendarStartDate = Calendar.getInstance();
                calendarEndDate = Calendar.getInstance();
            }
        });

        deleteIconTime.setOnClickListener(new View.OnClickListener() {
            //Clear data
            @Override
            public void onClick(View v) {
                textViewStartTime.setText("");
                textViewEndTime.setText("");
                calendarStartTime = Calendar.getInstance();
                calendarEndTime = Calendar.getInstance();
            }
        });

        deleteIconGps.setOnClickListener(new View.OnClickListener() {
            //Clear data
            @Override
            public void onClick(View v) {
                textViewGpsCoords.setText("");
                latitude = 0;
                longitude = 0;
                textViewRadius.setText("");
            }
        });

        deleteIconCompass.setOnClickListener(new View.OnClickListener() {
            //Clear data
            @Override
            public void onClick(View v) {
                textViewDirection.setText("");
                azimuth = 0;
                textViewDirectionOffset.setText("");
            }
        });

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if there are no errors update SharedPreferences with new Data
                if (checkForErrors()) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    SharedPreferences.Editor editor = preferences.edit();
                    if (dateFlag) {
                        editor.putBoolean(PREF_KEY_DATE_FLAG, dateFlag);
                        editor.putLong(PREF_KEY_START_DATE, calendarStartDate.getTime().getTime());
                        editor.putLong(PREF_KEY_END_DATE, calendarEndDate.getTime().getTime());
                    }
                    else {
                        editor.putBoolean(PREF_KEY_DATE_FLAG, dateFlag);
                    }
                    if (timeFlag) {
                        editor.putBoolean(PREF_KEY_TIME_FLAG, timeFlag);
                        editor.putLong(PREF_KEY_START_TIME, calendarStartTime.getTime().getTime());
                        editor.putLong(PREF_KEY_END_TIME, calendarEndTime.getTime().getTime());
                    }
                    else {
                        editor.putBoolean(PREF_KEY_TIME_FLAG, timeFlag);
                    }
                    if (gpsFlag) {
                        editor.putBoolean(PREF_KEY_GPS_FLAG, gpsFlag);
                        editor.putFloat(PREF_KEY_GPS_LAT, (float)latitude);
                        editor.putFloat(PREF_KEY_GPS_LNG, (float)longitude);
                        editor.putInt(PREF_KEY_GPS_RADIUS, Integer.parseInt(textViewRadius.getText().toString()));
                    }
                    else {
                        editor.putBoolean(PREF_KEY_GPS_FLAG, gpsFlag);
                    }
                    if (orientationFlag) {
                        editor.putBoolean(PREF_KEY_ORIENTATION_FLAG, orientationFlag);
                        editor.putFloat(PREF_KEY_COMPASS_DIR, azimuth);
                        editor.putInt(PREF_KEY_COMPASS_OFFSET, Integer.parseInt(textViewDirectionOffset.getText().toString()));
                    }
                    else {
                        editor.putBoolean(PREF_KEY_ORIENTATION_FLAG, orientationFlag);
                    }
                    //Commit changes before closing activity
                    editor.commit();
                    Intent intent = new Intent();
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Load current FilterValues
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        if (sharedPreferences.getBoolean(PREF_KEY_DATE_FLAG, false)) {
            calendarStartDate.setTime(new Date(sharedPreferences.getLong(PREF_KEY_START_DATE, 0)));
            calendarEndDate.setTime(new Date(sharedPreferences.getLong(PREF_KEY_END_DATE, 0)));
            String format = "dd/MM/yy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
            textViewStartDate.setText(simpleDateFormat.format(calendarStartDate.getTime()));
            textViewEndDate.setText(simpleDateFormat.format(calendarEndDate.getTime()));
        }
        if (sharedPreferences.getBoolean(PREF_KEY_TIME_FLAG, false)) {
            calendarStartTime.setTime(new Date(sharedPreferences.getLong(PREF_KEY_START_TIME, 0)));
            calendarEndTime.setTime(new Date(sharedPreferences.getLong(PREF_KEY_END_TIME, 0)));
            String format = "HH:mm";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
            textViewStartTime.setText(simpleDateFormat.format(calendarStartTime.getTime())+ " h");
            textViewEndTime.setText(simpleDateFormat.format(calendarEndTime.getTime())+ " h");
        }
        if (sharedPreferences.getBoolean(PREF_KEY_GPS_FLAG, false)) {
            latitude = (double) sharedPreferences.getFloat(PREF_KEY_GPS_LAT, 0);
            longitude = (double) sharedPreferences.getFloat(PREF_KEY_GPS_LNG, 0);
            textViewGpsCoords.setText(String.format("%.1f", latitude)+" / "+String.format("%.1f", longitude));
            textViewRadius.setText(sharedPreferences.getInt(PREF_KEY_GPS_RADIUS, 0)+"");
        }
        if (sharedPreferences.getBoolean(PREF_KEY_ORIENTATION_FLAG, false)) {
            azimuth = sharedPreferences.getFloat(PREF_KEY_COMPASS_DIR, 0);
            textViewDirection.setText(String.format("%.0f", azimuth) + "°");
            textViewDirectionOffset.setText(sharedPreferences.getInt(PREF_KEY_COMPASS_OFFSET, 0)+"");
        }
    }

    private boolean checkForErrors() {
        //Date Check
        if (textViewStartDate.getText().toString().equals("") && textViewEndDate.getText().toString().equals("")) {
            //both empty
            dateFlag = false;
        }
        else if (!textViewStartDate.getText().toString().equals("") && !textViewEndDate.getText().toString().equals("")) {
            //both filled
            if (calendarStartDate.getTimeInMillis() <= calendarEndDate.getTimeInMillis()) {
                //no errors
                dateFlag = true;
            }
            else {
                Toast.makeText(getContext(), "StartDate has to be before EndDate", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        else {
            Toast.makeText(getContext(), "Fill out both Date Fields", Toast.LENGTH_LONG).show();
            return false;
        }

        //Time Check
        if (textViewStartTime.getText().toString().equals("") && textViewEndTime.getText().toString().equals("")) {
            //both empty
            timeFlag = false;
        }
        else if (!textViewStartTime.getText().toString().equals("") && !textViewEndTime.getText().toString().equals("")) {
            //both filled
            timeFlag = true;
        }
        else {
            Toast.makeText(getContext(), "Fill out both Time Fields", Toast.LENGTH_LONG).show();
            return false;
        }

        //Gps Check
        if (textViewGpsCoords.getText().toString().equals("") && textViewRadius.getText().toString().equals("")) {
            //both empty
            gpsFlag = false;
        }
        else if (!textViewGpsCoords.getText().toString().equals("") && !textViewRadius.getText().toString().equals("")) {
            //both filled
            if (Float.parseFloat(textViewRadius.getText().toString()) > 20000000) {
                //Radius*2 cant be bigger than earth circumference (~40 000+ km)
                Toast.makeText(getContext(), "Radius can't be over 20,000km", Toast.LENGTH_LONG).show();
                return false;
            }
            else if (textViewRadius.getText().toString().equals("0")) {
                Toast.makeText(getContext(), "Radius can't be 0", Toast.LENGTH_LONG).show();
                return false;
            }
            else {
                //no errors
                gpsFlag = true;
            }
        }
        else {
            Toast.makeText(getContext(), "Fill out both GpsCoord and Radius Fields", Toast.LENGTH_LONG).show();
            return false;
        }

        //Direction Check
        if (textViewDirection.getText().toString().equals("") && textViewDirectionOffset.getText().toString().equals("")) {
            //both empty
            orientationFlag = false;
        }
        else if (!textViewDirection.getText().toString().equals("") && !textViewDirectionOffset.getText().toString().equals("")) {
            //both filled
            if (Float.parseFloat(textViewDirectionOffset.getText().toString()) > 179) {
                //Direction Span should be smaller value than 360
                Toast.makeText(getContext(), "Max Direction Offset is 179°", Toast.LENGTH_LONG).show();
                return false;
            }
            else if (textViewDirectionOffset.getText().toString().equals("0")) {
                Toast.makeText(getContext(), "Direction Offset can't be 0", Toast.LENGTH_LONG).show();
                return false;
            }
            else {
                //no errors
                orientationFlag = true;
            }
        }
        else {
            Toast.makeText(getContext(), "Fill out both Compass Dir. and Offset Fields", Toast.LENGTH_LONG).show();
            return false;
        }

        //checks passed and all flags are set
        return true;
    }

    @Override
    public void selectedAzimuth(float azimuth) {
        //retrieve current compass direction
        this.azimuth = azimuth;
        textViewDirection.setText(String.format("%.0f", azimuth) + "°");
    }
}
