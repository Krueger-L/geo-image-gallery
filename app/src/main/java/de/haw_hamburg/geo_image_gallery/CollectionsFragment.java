package de.haw_hamburg.geo_image_gallery;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CollectionsFragment extends Fragment implements CollectionsAdapter.OnItemClickListener{

    private TextView emptyTextView;
    private RecyclerView recyclerView;
    private PhotoManager photoManager;
    private CollectionsAdapter adapter;

    public CollectionsFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        //setup PhotoManager
        photoManager = new PhotoManager(getActivity());
        photoManager.loadCollection();
        photoManager.loadSavedPhotos();

        //create CollectionsAdapter
        adapter = new CollectionsAdapter(getContext());
        adapter.setOnItemClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_collections, container, false);
        emptyTextView = (TextView) rootView.findViewById(R.id.emptyTextView);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        //setup adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onResume() {
        //update PhotoManager content
        photoManager.update();
        //update adapter content
        adapter.setCollections(photoManager.collections.getCollections());
        //update Layout according to adapter content
        if (adapter.getItemCount() > 0) {
            emptyTextView.setVisibility(View.GONE);
        }
        else {
            emptyTextView.setVisibility(View.VISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.collections_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add:
                //create new AlertDialog for collection creation
                final EditText editText = new EditText(getActivity());
                AlertDialog dia = new AlertDialog.Builder(getActivity())
                        .setTitle("Create new Collection")
                        .setView(editText)
                        .setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String tmpPath = editText.getText().toString();
                                boolean stringEmpty = true;
                                if (tmpPath.trim().length() > 0) {
                                    stringEmpty = false;
                                }
                                if (stringEmpty) {
                                    Toast.makeText(getActivity(), "Collection must have a name", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    boolean passed = photoManager.collections.addCollection(tmpPath);
                                    if (passed) {
                                        photoManager.saveCollection();

                                        adapter.setCollections(photoManager.collections.getCollections());
                                        if (adapter.getItemCount() > 0) {
                                            emptyTextView.setVisibility(View.GONE);
                                        }
                                        else {
                                            emptyTextView.setVisibility(View.VISIBLE);
                                        }
                                        Toast.makeText(getActivity(), "Collection saved", Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        Toast.makeText(getActivity(), "Collection already exists", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        })
                        .create();
                dia.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClicked(Collection collection) {
        //start CollectionsDetailActivity
        Intent intent = CollectionsDetailActivity.getStartActivityIntent(this.getContext(), collection.getName());
        startActivityForResult(intent, CollectionsDetailActivity.REQUEST_COLLECTIONS_DETAIL_ACTIVITY);
    }

    @Override
    public void onDeleteClicked(final Collection collection) {
        //create new AlterDialog for collection deletion
        AlertDialog deleteDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Delete Collection?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        photoManager.collections.deleteCollection(collection.getName());
                        photoManager.saveCollection();
                        adapter.setCollections(photoManager.collections.getCollections());
                        if (adapter.getItemCount() > 0) {
                            emptyTextView.setVisibility(View.GONE);
                        }
                        else {
                            emptyTextView.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                })
                .create();
        deleteDialog.show();
    }
}
