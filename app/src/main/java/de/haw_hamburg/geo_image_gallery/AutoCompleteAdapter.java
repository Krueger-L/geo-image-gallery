package de.haw_hamburg.geo_image_gallery;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter extends RecyclerView.Adapter<AutoCompleteAdapter.ViewHolder> {

    private List<AutoCompletePrediction> predictions;
    private OnItemClickListener listener;


    public AutoCompleteAdapter()
    {
        this.predictions = new ArrayList<>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.autocomplete_list_item,
                parent,
                false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        AutoCompletePrediction prediction = predictions.get(position);
        holder.setPlaceAutocompletePrediction(prediction);
        holder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                listener.onItemClicked(holder.prediction);
            }
        });
    }


    @Override
    public int getItemCount()
    {
        return predictions.size();
    }


    public void setPlaceAutocompletePredictions(List<AutoCompletePrediction> predictions)
    {
        this.predictions = predictions;
        notifyDataSetChanged();
    }


    public AutoCompletePrediction getPlaceAutocompletePrediction(int position)
    {
        return predictions.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        this.listener = listener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private View view;
        private TextView tvDescription;
        private AutoCompletePrediction prediction;


        public ViewHolder(View itemView)
        {
            super(itemView);
            this.view = itemView;
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
        }


        public void setOnClickListener(View.OnClickListener listener)
        {
            view.setOnClickListener(listener);
        }


        public void setPlaceAutocompletePrediction(AutoCompletePrediction prediction)
        {
            this.prediction = prediction;
            tvDescription.setText(this.prediction.getDescription());
        }
    }

    public interface OnItemClickListener
    {
        void onItemClicked(AutoCompletePrediction prediction);
    }
}
