package de.haw_hamburg.geo_image_gallery;


import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.net.URI;

public class PhotoClusterItem implements ClusterItem {

    private LatLng latLng;
    private URI uri;

    public PhotoClusterItem(LatLng latLng, URI uri) {
        this.latLng = latLng;
        this.uri = uri;
    }

    public URI getUri() {
        return uri;
    }

    @Override
    public LatLng getPosition() {
        return latLng;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
