package de.haw_hamburg.geo_image_gallery;


import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.ArrayList;

public class FilterMapFragment extends Fragment implements OnMapReadyCallback {

    private SearchView searchView;
    private RecyclerView searchRecyclerView;
    private RelativeLayout rootLayout;
    private AutoCompleteAdapter autoCompleteAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private PlaceAPI placeAPI;

    private MapView mapView;
    private GoogleMap googleMap;
    private Marker marker;

    private double latitude;
    private double longitude;

    public FilterMapFragment() {

    }

    public static FilterMapFragment newInstanceWithBundle(double lat, double lng) {
        FilterMapFragment fragment = new FilterMapFragment();
        Bundle args = new Bundle();
        args.putDouble(FilterMapActivity.INTENT_EXTRA_PARAM_LAT, lat);
        args.putDouble(FilterMapActivity.INTENT_EXTRA_PARAM_LNG, lng);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            latitude = getArguments().getDouble(FilterMapActivity.INTENT_EXTRA_PARAM_LAT, 0);
            longitude = getArguments().getDouble(FilterMapActivity.INTENT_EXTRA_PARAM_LNG, 0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filter_map, container, false);
        rootLayout = (RelativeLayout) rootView.findViewById(R.id.root_Layout);
        placeAPI = new PlaceAPI();
        final Button cancelButton = (Button) rootView.findViewById(R.id.cancel_button);
        final Button applyButton = (Button) rootView.findViewById(R.id.apply_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close activity
                getActivity().finish();
            }
        });
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (marker != null) {
                    //set result data and close activity
                    Intent intent = new Intent();
                    intent.putExtra(FilterMapActivity.INTENT_EXTRA_PARAM_LAT, marker.getPosition().latitude);
                    intent.putExtra(FilterMapActivity.INTENT_EXTRA_PARAM_LNG, marker.getPosition().longitude);
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();
                }
                else {
                    Toast.makeText(getContext(), "Need to place Marker first", Toast.LENGTH_LONG).show();
                }
            }
        });
        mapView = (MapView) rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        autoCompleteAdapter = new AutoCompleteAdapter();
        autoCompleteAdapter.setOnItemClickListener(new AutoCompleteAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(AutoCompletePrediction prediction) {
                //autocomplete item clicked -> process corresponding Google Place ID
                hideSoftKeyboard(getActivity());
                searchView.setQuery(prediction.getDescription(), false);
                new GetPredictionDetailsTask().execute(prediction.getPlaceId());
            }
        });
        layoutManager = new LinearLayoutManager(getActivity());
        searchRecyclerView = (RecyclerView) view.findViewById(R.id.rvPlaceAutocompletePredictions);
        searchRecyclerView.setHasFixedSize(true);
        searchRecyclerView.setLayoutManager(layoutManager);
        searchRecyclerView.setAdapter(autoCompleteAdapter);
        searchRecyclerView.setItemAnimator(new DefaultItemAnimator());

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.filter_map_fragment_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        //Set new ClearButtonIcon
        ImageView clearIcon = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        clearIcon.setImageResource(R.drawable.ic_cancel_24dp);

        //setup searchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getCallingActivity()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //start GetPredictionTask to get new autocomplete list
                new GetPredictionsTask().execute(newText, "");

                return true;
            }
        });
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //hide/show autocomplete list layout
                if (hasFocus == false) {
                    searchRecyclerView.setVisibility(View.GONE);
                }
                else {
                    searchRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        searchView.onActionViewExpanded();
        searchView.setQuery("", false);
        searchView.clearFocus();

        super.onCreateOptionsMenu(menu, inflater);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        this.googleMap = map;
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        //place marker if there are valid coordinates
        if (latitude != 0 && longitude != 0) {
            marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10));
        }
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //return true so default behaviour is not triggered
                return true;
            }
        });
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //replace Marker
                googleMap.clear();
                marker = googleMap.addMarker(new MarkerOptions().position(latLng));
                latitude = latLng.latitude;
                longitude = latLng.longitude;
                searchView.setQuery("", false);
            }
        });
    }

    private class GetPredictionsTask extends AsyncTask<String, Void, ArrayList<AutoCompletePrediction>> {

        //Network Task
        //get GooglePlaces AutoComplete list
        //strings[0] = searchInput string, strings[1] = optionParameter

        protected ArrayList<AutoCompletePrediction> doInBackground(String... strings) {
            return placeAPI.autocomplete(strings[0], strings[1]);
        }

        protected void onPostExecute(ArrayList<AutoCompletePrediction> autoCompletePredictions) {
            autoCompleteAdapter.setPlaceAutocompletePredictions(autoCompletePredictions);
        }
    }

    private class GetPredictionDetailsTask extends AsyncTask<String, Void, JSONObject> {

        //Network Task
        //get GooglePlaces Details for given Google Places ID -> set new Marker on map

        protected JSONObject doInBackground(String... strings) {
            return placeAPI.details(strings[0]);
        }

        protected void onPostExecute(JSONObject jsonObject) {
            LatLng lng = placeAPI.getLatLngFromDetails(jsonObject);
            rootLayout.requestFocus();
            googleMap.clear();
            marker = googleMap.addMarker(new MarkerOptions().position(lng));
            latitude = lng.latitude;
            longitude = lng.longitude;
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lng, 10));
        }


    }
}
