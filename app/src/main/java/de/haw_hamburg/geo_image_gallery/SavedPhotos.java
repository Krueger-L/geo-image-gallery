package de.haw_hamburg.geo_image_gallery;

import java.io.Serializable;
import java.util.ArrayList;


public class SavedPhotos implements Serializable {

    private ArrayList<Photo> arrayList = new ArrayList<Photo>();

    public ArrayList<Photo> getArrayList() {
        return arrayList;
    }

    public void addItem(Photo photo) {
        arrayList.add(photo);
    }

    public void removePhoto(Photo photo) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (photo.getURI().getPath().equals(arrayList.get(i).getURI().getPath())) {
                arrayList.remove(i);
            }
        }
    }

    public boolean hasPhoto(Photo photo) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (photo.getURI().getPath().equals(arrayList.get(i).getURI().getPath())) {
                return true;
            }
        }
        return false;
    }

    public Photo getPhoto(String path) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (path.equals(arrayList.get(i).getURI().getPath())) {
                return arrayList.get(i);
            }
        }
        return null;
    }

    public void refreshList(ArrayList<Photo> availablePhotos) {
        //remove photos that are not on the device anymore
        ArrayList<Photo> deletionList = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            boolean present = false;
            for (int y = 0; y < availablePhotos.size(); y++) {
                if (arrayList.get(i).getURI().getPath().equals(availablePhotos.get(y).getURI().getPath())) {
                    present = true;
                }
            }
            if (!present) {
                deletionList.add(arrayList.get(i));
            }
        }
        for (int i = 0; i < deletionList.size(); i++) {
            removePhoto(deletionList.get(i));
        }
    }
}
