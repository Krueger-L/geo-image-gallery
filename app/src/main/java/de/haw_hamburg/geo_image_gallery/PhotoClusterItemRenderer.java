package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import jp.wasabeef.glide.transformations.CropSquareTransformation;

public class PhotoClusterItemRenderer  extends DefaultClusterRenderer<PhotoClusterItem> {

    private Context context;
    private GoogleMap googleMap;
    private ClusterManager<PhotoClusterItem> clusterManager;
    private IconGenerator iconGenerator;
    private IconGenerator photoIconGenerator;
    private BitmapDescriptor defaultIcon;

    public PhotoClusterItemRenderer(Context context, GoogleMap googleMap, ClusterManager<PhotoClusterItem> clusterManager) {
        super(context, googleMap, clusterManager);
        this.context = context;
        this.googleMap = googleMap;
        this.clusterManager = clusterManager;
        iconGenerator = new IconGenerator(context);
        photoIconGenerator = new IconGenerator(context);
        defaultIcon = BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon());
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<PhotoClusterItem> cluster) {
        //create cluster if there are more than 1 items
        return cluster.getSize() > 1;
    }

    @Override
    protected void onBeforeClusterItemRendered(PhotoClusterItem item, final MarkerOptions markerOptions) {
        //use default icon before rendering
        markerOptions.icon(defaultIcon);
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<PhotoClusterItem> cluster, MarkerOptions markerOptions) {
        //create cluster icon based on item count
        Bitmap bitmap = iconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
    }

    @Override
    protected void onClusterItemRendered(PhotoClusterItem clusterItem, final Marker marker) {
        //create icon with image
        if (marker.getTag() == null) {
            marker.setTag("ddd");
        }
        MultiTransformation multiTransformation = new MultiTransformation(
                new CropSquareTransformation()
        );
        GlideApp.with(context)
                .asBitmap()
                .load(clusterItem.getUri().getPath())
                .apply(RequestOptions.bitmapTransform(multiTransformation))
                .into(new SimpleTarget<Bitmap>(150, 150) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        ImageView imageView = new ImageView(context);
                        imageView.setImageBitmap(resource);
                        photoIconGenerator.setContentView(imageView);
                        BitmapDescriptor markerIcon = BitmapDescriptorFactory.fromBitmap(photoIconGenerator.makeIcon());

                        if (marker.getTag() != null) {
                            //Validate that marker is still available before setting icon
                            marker.setIcon(markerIcon);
                        }
                    }
                });
    }
}
