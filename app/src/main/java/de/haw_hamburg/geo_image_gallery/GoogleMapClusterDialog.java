package de.haw_hamburg.geo_image_gallery;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;

import java.util.ArrayList;

public class GoogleMapClusterDialog extends DialogFragment implements PhotoListAdapter.PhotoClickListener {

    private RecyclerView recyclerView;
    private ArrayList<Photo> photos = new ArrayList<>();
    private PhotoListAdapter adapter;
    private GoogleMapClusterDialogListener listener;

    public GoogleMapClusterDialog() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            listener = (GoogleMapClusterDialogListener) getParentFragment();
        } catch (ClassCastException e) {
            throw  new ClassCastException(getParentFragment().toString() + "must implement GoogleMapClusterDialogListener");
        }
    }

    public interface GoogleMapClusterDialogListener {
        public void requestDialogPhotos();
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
        adapter.setData(this.photos);
    }

    @Override
    public void onPhotoClicked(View view, int position) {
        //start PhotoDetailActivity and close dialog
        Photo photo = adapter.getPhoto(position);
        Intent intent = PhotoDetailActivity.getStartActivityIntent(getActivity(), photo.getURI().getPath(), photo.getDate(), photo.getLatitude(), photo.getLongitude(), photo.getDirection());
        startActivity(intent);
        dismiss();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_googlemap_cluster, container, false);

        //setup recyclerview basics
        recyclerView = (RecyclerView)rootView.findViewById(R.id.photo_list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);

        final Button button = (Button) rootView.findViewById(R.id.cluster_dialog_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close dialog
                dismiss();
            }
        });

        //create PhotoListAdapter
        adapter = new PhotoListAdapter(getContext(), photos, PhotoListAdapter.ADAPTER_TYPE_NORMAL);
        adapter.setPhotoClickListener(this);
        //setup recycler with Glide preloader
        RecyclerViewPreloader<Photo> preloader = new RecyclerViewPreloader<>(GlideApp.with(getContext()), adapter, adapter.getPreloadSizeProvider(), 20);
        recyclerView.addOnScrollListener(preloader);
        recyclerView.setItemViewCacheSize(0);
        //set PhotoListAdapter to recyclerview
        recyclerView.setAdapter(adapter);
        //request Photos for adapter from parent
        listener.requestDialogPhotos();

        return rootView;
    }

}
