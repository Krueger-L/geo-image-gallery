package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.ClusterManager;

import java.net.URI;
import java.util.ArrayList;

import de.haw_hamburg.geo_image_gallery.ExtResources.ModifiedAlgorithm;

public class CollectionsAdapter extends RecyclerView.Adapter<CollectionsAdapter.ViewHolder> {

    private ArrayList<Collection> collections;
    private OnItemClickListener listener;
    private Context context;

    public CollectionsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.collections_list_item,
                parent,
                false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        Collection collection = collections.get(position);
        holder.setCollection(collection);
        holder.setCollectionName(collection.getName());
        if (holder.googleMap != null) {
            holder.onMapReady(holder.googleMap);
        }
        holder.setOnViewClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                listener.onItemClicked(holder.collection);
            }
        });
        holder.setOnDeleteClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDeleteClicked(holder.collection);
            }
        });
    }


    @Override
    public int getItemCount()
    {
        return collections.size();
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.googleMap != null) {
            holder.googleMap.clear();
            holder.googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        }
    }


    public void setCollections(ArrayList<Collection> collections)
    {
        this.collections = collections;
        notifyDataSetChanged();
    }


    public Collection getCollection(int position)
    {
        return collections.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        this.listener = listener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

        private View view;
        private GoogleMap googleMap;
        private MapView mapView;
        private Collection collection;
        private TextView collectionName;
        private ImageView deleteButton;

        public ViewHolder(View itemView)
        {
            super(itemView);
            this.view = itemView;
            mapView = (MapView) itemView.findViewById(R.id.mapView);
            collectionName = (TextView) itemView.findViewById(R.id.collectionName);
            deleteButton = (ImageView) itemView.findViewById(R.id.delete);

            if (mapView != null) {
                mapView.onCreate(null);
                mapView.onResume();
                mapView.getMapAsync(this);
            }
        }


        public void setOnViewClickListener(View.OnClickListener listener) {
            view.setOnClickListener(listener);
        }

        public void setOnDeleteClickListener(View.OnClickListener listener) {
            deleteButton.setOnClickListener(listener);
        }


        public void setCollection(Collection collection) {
            this.collection = collection;
        }

        public void setCollectionName(String name) {
            collectionName.setText(name);
        }

        @Override
        public void onMapReady(GoogleMap map) {
            //setup GoogleMap
            MapsInitializer.initialize(context.getApplicationContext());
            this.googleMap = map;
            googleMap.getUiSettings().setMapToolbarEnabled(false);
            googleMap.getUiSettings().setAllGesturesEnabled(false);
            mapView.setClickable(false);
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            //apply padding to top of map to compensate for marker layout height
            googleMap.setPadding(0, 250, 0, 0);

            //setup ClusterManager
            ClusterManager clusterManager = new ClusterManager<PhotoClusterItem>(context.getApplicationContext(), googleMap);
            clusterManager.setAlgorithm(new ModifiedAlgorithm<PhotoClusterItem>());
            clusterManager.setRenderer(new PhotoClusterItemRenderer(context.getApplicationContext(), googleMap, clusterManager));

            //add items to map and generate bounds
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (int i = 0; i < collection.getList().size(); i++) {
                double latitude = (double) collection.getList().get(i).getLatitude();
                double longitude = (double) collection.getList().get(i).getLongitude();
                URI uri = collection.getList().get(i).getURI();
                clusterManager.addItem(new PhotoClusterItem(new LatLng(latitude, longitude), uri));
                builder.include(new LatLng(latitude, longitude));
            }
            if (collection.getList().size() > 1) {
                //Zoom in with bounds
                LatLngBounds bounds = builder.build();
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                clusterManager.cluster();
            }
            else if (collection.getList().size() == 1) {
                //medium Zoom
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng((double)collection.getList().get(0).getLatitude(), (double)collection.getList().get(0).getLongitude()), 10));
                clusterManager.cluster();
            }
            else {
                //Zoom out
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0, 0), 0));
            }
        }
    }

    public interface OnItemClickListener
    {
        void onItemClicked(Collection collection);
        void onDeleteClicked(Collection collection);
    }
}
