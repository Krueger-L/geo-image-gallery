package de.haw_hamburg.geo_image_gallery;


import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

public class PlaceAPI {
    private static final String TAG = PlaceAPI.class.getSimpleName();

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_DETAILS = "/details";
    private static final String OUT_JSON = "/json";

    private String systemLanguage = Locale.getDefault().getLanguage();

    private static final String API_KEY = "AIzaSyC_oT85dluqJRGqHF0YlujIeedip7Q_HV8";

    public JSONObject details (String placeId){
        JSONObject result = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAILS + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&placeid=" + placeId);
            sb.append("&language=" + systemLanguage);

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return result;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return result;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            result = jsonObj.getJSONObject("result");

        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return result;
    }

    public LatLng getLatLngFromDetails(JSONObject jsonObject) {
        LatLng latLng = new LatLng(0, 0);
        try {
            JSONObject object = jsonObject.getJSONObject("geometry").getJSONObject("location");
            latLng = new LatLng(object.getDouble("lat"), object.getDouble("lng"));
        } catch (JSONException e) {
            Log.e(PlaceAPI.class.getSimpleName(), "Cannot process JSON Request");
        }
        return latLng;
    }

    public ArrayList<AutoCompletePrediction> autocomplete (String input, String params) {
        ArrayList<AutoCompletePrediction> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append(params);
            sb.append("&language=" + systemLanguage);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<AutoCompletePrediction>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                AutoCompletePrediction autoCompletePrediction = new AutoCompletePrediction(predsJsonArray.getJSONObject(i).getString("place_id"), predsJsonArray.getJSONObject(i).getString("description"));
                resultList.add(autoCompletePrediction);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }
}
