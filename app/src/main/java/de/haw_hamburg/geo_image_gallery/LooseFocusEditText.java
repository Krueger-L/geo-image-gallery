package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;



public class LooseFocusEditText extends AppCompatEditText {

    public LooseFocusEditText(Context context) {
        super(context);
    }

    public LooseFocusEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LooseFocusEditText(Context context, AttributeSet attributeSet, int def_style_attribute) {
        super(context, attributeSet, def_style_attribute);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent keyEvent) {
        //Loose Focus on Back-Key Pressed while PopupKeyboard is active
        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
            this.clearFocus();
        }
        return super.onKeyPreIme(keyCode, keyEvent);
    }
}
