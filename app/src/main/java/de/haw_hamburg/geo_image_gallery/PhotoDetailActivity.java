package de.haw_hamburg.geo_image_gallery;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

public class PhotoDetailActivity extends AppCompatActivity implements PhotoDetailFragment.PhotoDetailFragmentListener{

    public static final  int REQUEST_PHOTO_DETAIL_ACTIVITY = 1004;
    public static final String INTENT_EXTRA_PATH = "intent_extra_path";
    public static final String INTENT_EXTRA_DATE = "intent_extra_date";
    public static final String INTENT_EXTRA_LAT = "intent_extra_lat";
    public static final String INTENT_EXTRA_LNG = "intent_extra_lng";
    public static final String INTENT_EXTRA_DIR = "intent_extra_dir";

    public static Intent getStartActivityIntent(Context context, String path, Long date, float latitude, float longitude, float direction) {
        Intent intent = new Intent(context, PhotoDetailActivity.class);
        intent.putExtra(INTENT_EXTRA_PATH, path);
        intent.putExtra(INTENT_EXTRA_DATE, date);
        intent.putExtra(INTENT_EXTRA_LAT, latitude);
        intent.putExtra(INTENT_EXTRA_LNG, longitude);
        intent.putExtra(INTENT_EXTRA_DIR, direction);
        return intent;
    }

    public static void startActivity(Context context, String path, Long date, float latitude, float longitude, float direction) {
        context.startActivity(getStartActivityIntent(context, path, date, latitude, longitude, direction));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
            //add fragment to layout container
            PhotoDetailFragment fragment = PhotoDetailFragment.newInstanceWithBundle(getIntent().getStringExtra(INTENT_EXTRA_PATH), getIntent().getLongExtra(INTENT_EXTRA_DATE, 0), getIntent().getFloatExtra(INTENT_EXTRA_LAT, 0), getIntent().getFloatExtra(INTENT_EXTRA_LNG, 0), getIntent().getFloatExtra(INTENT_EXTRA_DIR, 0));
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideStatusBar();
        showActionBar(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showActionBar(boolean bool) {
        if (bool) {
            getSupportActionBar().show();
        }
        else {
            getSupportActionBar().hide();
        }
    }

    @Override
    public void hideStatusBar() {
        View decorView = getWindow().getDecorView();
        int uiOptionFullscreen = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptionFullscreen);
    }
}
