package de.haw_hamburg.geo_image_gallery;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements PhotosFragment.PhotosFragmentListener {

    private final String PHOTO_TAG = "PHOTOS";
    private final String COLLECTIONS_TAG = "COLLECTIONS";
    private PhotoManager photoManager;
    private Context context;
    private BottomNavigationView navigation;
    private PhotosFragment photosFragment;
    private CollectionsFragment collectionsFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_image:
                    showFragment(PHOTO_TAG);

                    return true;
                case R.id.navigation_collection:
                    showFragment(COLLECTIONS_TAG);

                    return true;
            }
            return false;
        }

    };

    public void requestPhotos() {
        PhotosFragment photosFragment = (PhotosFragment) getSupportFragmentManager().findFragmentByTag(PHOTO_TAG);
        if (photosFragment != null) {
            photosFragment.setAllPhotos(photoManager.getAllPhotos());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        context = this;

        //allow networking on main thread
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setSupportActionBar(toolbar);

        photoManager = new PhotoManager(this);
        photoManager.update();

        photosFragment = new PhotosFragment();
        collectionsFragment = new CollectionsFragment();

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, photosFragment, PHOTO_TAG)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        photoManager.update();
    }

    private void showFragment(String tag) {
        if (tag.equals(PHOTO_TAG)) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, photosFragment, PHOTO_TAG)
                    .commit();
        }
        else if (tag.equals(COLLECTIONS_TAG)) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, collectionsFragment, COLLECTIONS_TAG)
                    .commit();
        }
    }
}
