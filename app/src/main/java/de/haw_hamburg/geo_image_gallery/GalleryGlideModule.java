package de.haw_hamburg.geo_image_gallery;


import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

//Create Glide's "generated API"

@GlideModule
public class GalleryGlideModule extends AppGlideModule {
    //empty
}
